library(testthat)

setwd("..")

source("R/practice.R")


context("r_practice test")
data(starwars)


x <- filterExampleFunction(starwars)

test_that("highest avg BMI is on Tatooine, lowest - Coruscant", {
    expect_equal(filterExampleFunction(starwars)$homeworld[[1]], "Tatooine")
    expect_equal(filterExampleFunction(starwars)$homeworld[[9]], "Coruscant")
})

test_that("avg BMI on Tatooine is 29.3", {
    expect_equal(round(filterExampleFunction(starwars)[filterExampleFunction(starwars)$homeworld=='Tatooine', ][["Average_BMI"]], digits = 1), 29.3)
    expect_equal(round(mean(filterExampleFunction(starwars)$Average_BMI), digits = 1), 21.5)
})


    